<form action="admin.php"
        method="POST">
<input type="hidden" name="form_id" value="<?php print $row['form_id']; ?>" />
<label id="label_for_name">
    Имя<br />
    <?php $row['name'] = htmlspecialchars($row['name'], ENT_QUOTES) ?>
    <input name="name" value="<?php print $row['name']; ?>"/>
</label><br />

<label id="label_for_email">
    E-mail:<br />
    <?php $row['email'] = htmlspecialchars($row['email'], ENT_QUOTES) ?>
    <input name="email" value="<?php print $row['email']; ?>" type="email" />
</label><br />

<label id="label_for_birthday">
    Дата рождения:<br />
    <input name="birthday" value="<?php print $row['birthday']; ?>" type="date" /> 
</label><br />

Пол:<br />
<label><input type="radio"
    name="gender" value="M" <?php if ($row['gender'] == 'M') {print 'checked';} ?>/>
    Мужской
</label>
<label><input type="radio"
    name="gender" value="F" <?php if ($row['gender'] == 'F') {print 'checked';} ?> />
    Женский
</label>
<label><input type="radio"
    name="gender" value="NB" <?php if ($row['gender'] == 'NB') {print 'checked';} ?> />
    Небинарная персона
</label><br><br>
Количество конечностей:<br />
<label><input type="radio" <?php if ($row['limb_number'] == '0') {print 'checked';} ?>
    name="limb_number" value="0" />
    0
</label>
<label><input type="radio" <?php if ($row['limb_number'] == '1') {print 'checked';} ?>
    name="limb_number" value="1" />
    1
</label>
<label><input type="radio" <?php if ($row['limb_number'] == '2') {print 'checked';} ?>
    name="limb_number" value="2" />
    2
</label>
<label><input type="radio" <?php if ($row['limb_number'] == '3') {print 'checked';} ?>
    name="limb_number" value="3" />
    3
</label>
<label><input type="radio" <?php if ($row['limb_number'] == '4') {print 'checked';} ?>
    name="limb_number" value="4" />
    4
</label>
<label><input type="radio" <?php if ($row['limb_number'] == '5') {print 'checked';} ?>
    name="limb_number" value="5" />
    Другое
</label>
<br><br>
<label>
    Сверхспособности:
    <br />
    <select name="superpowers[]"
    multiple="multiple">
    <option value="1" <?php if ($superpowers[0]) {print 'selected';} ?>>Бессмертие</option>
    <option value="2" <?php if ($superpowers[1]) {print 'selected';} ?>>Прохождение сквозь стены</option>
    <option value="3" <?php if ($superpowers[2]) {print 'selected';} ?>>Левитация</option>
    </select>
</label>
<br><br>
<label>
    Биография:<br />
    <?php $row['biography'] = htmlspecialchars($row['biography'], ENT_QUOTES) ?>
    <textarea class="message" rows="7" cols="50" name="biography"><?php print $row['biography']; ?></textarea>
</label>
<br><br>
<input id="submit" type="submit" value="Изменить" name="update" />
<input id="submit" type="submit" value="Удалить" name="delete"/>
</form>
